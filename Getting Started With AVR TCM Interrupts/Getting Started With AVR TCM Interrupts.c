/*
 * Getting_Started_With_AVR_TCM_Interrupts.c
 *
 * Created: 3/28/2015 0:54:52
 *  Author: Brandy
 */ 


#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

ISR(TIMER1_COMPA_vect)
{
	PINB |= (1<<PINB0);
}

void Timer_Frequency(int freq)
{
	TCCR1 |=(1<<CTC1)|(0 << CS13)|(0 << CS12)|(1 << CS11)|(1 << CS10);
	/*
		TCCR1 � Timer/Counter1 Control Register
		CTC1 PWM1A COM1A1 COM1A0 CS13 CS12 CS11 CS10
		Clear Timer/Counter on Compare Match
			When the CTC1 control bit is set (one), Timer/Counter1 is reset to $00 
			in the CPU clock cycle after a compare match with OCR1C register value. 
			If the control bit is cleared, Timer/Counter1 continues counting and is unaffected by a compare match.
		Pulse Width Modulator A Enable
		Comparator A Output Mode, Bits 1 and 0
		Clock Select Bits 3, 2, 1, and 0
			The Clock Select bits 3, 2, 1, and 0 define the prescaling source of Timer/Counter1.
			CS11 and CS10 as 1 is the prescaler 102 so the Timer Will Overflow Every 1.907seconds
		pg89
	*/
	TIMSK |=(1 <<OCIE1A);
	/*
		Timer/Counter Interrupt Mask Register
		� OCIE1A OCIE1B OCIE0A OCIE0B TOIE1 TOIE0 � 
		Reserved Bit
		Timer/Counter1 Output Compare Interrupt Enable
			When the OCIE1A bit is set (one) and the I-bit in the Status Register is set (one), 
			the Timer/Counter1 Compare MatchA, interrupt is enabled. 
			The corresponding interrupt at vector $003 is executed if a compare matchA occurs.
			The Compare Flag in Timer/Counter1 is set (one) in the Timer/Counter Interrupt Flag Register.
		Timer/Counter1 Output Compare Interrupt Enable
		Timer/Counter1 Overflow Interrupt Enable
		Reserved Bit
		pg92
	*/
	OCR1A = F_CPU/(freq*2*256)-1;
	/*
		The Timer/Counter Output Compare Register A contains data to be continuously compared with Timer/Counter1.
		Actions on compare matches are specified in TCCR1. A compare match does only occur if Timer/Counter1 counts
		to the OCR1A value. 
	*/
}
int main(void)
{
	DDRB |= (1 << DDB0);
	DDRB &= ~(1 << DDB1);
	sei();
	Timer_Frequency(1);
    while(1)
    {
        //TODO:: Please write your application code 
    }
}